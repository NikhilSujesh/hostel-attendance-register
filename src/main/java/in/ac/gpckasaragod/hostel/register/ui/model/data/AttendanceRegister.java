/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.hostel.register.ui.model.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class AttendanceRegister {
    private String name;
    private Integer studentId;
    private String department;
    private Double attendance;

    public AttendanceRegister(String name, Integer studentId, String department, Double attendance) {
        this.name = name;
        this.studentId = studentId;
        this.department = department;
        this.attendance = attendance;
    }
    private static final Logger LOG = Logger.getLogger(AttendanceRegister.class.getName());

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Double getAttendance() {
        return attendance;
    }

    public void setAttendance(Double attendance) {
        this.attendance = attendance;
    }
    
    
}
